def get_vms_query(param_name):
    switcher = {
        "on_time_delivery_rate": """ 
            select jsonb_agg(t) from (
                select * from vms_purchaseorder
            ) as t ;
        """,
        "quality_rating_avg": """ 
            select jsonb_agg(t) from (
                select * from vms_vendor
            ) as t ;
        """,
        "average_response_time": """ """,
        "fulfillment_rate": """ """
    }
    return switcher.get(param_name, "nothing")
