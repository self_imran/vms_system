from django.db import models


class Vendor(models.Model):
    objects = None
    name = models.CharField(max_length=256, null=False)
    contact_details = models.TextField()
    address = models.TextField()
    vendor_code = models.CharField(unique=True, max_length=10, null=False)
    on_time_delivery_rate = models.FloatField()
    quality_rating_avg = models.FloatField()
    average_response_time = models.FloatField()
    fulfilment_rate = models.FloatField()


class PurchaseOrder(models.Model):
    objects = None
    po_number = models.CharField(unique=True, max_length=10, null=False)
    vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE)
    order_date = models.DateTimeField()
    items = models.JSONField()
    quantity = models.IntegerField(null=False)
    status = models.CharField(null=False, max_length=20)
    quality_rating = models.FloatField()
    issue_date = models.DateTimeField(null=False)
    acknowledgment_date = models.DateTimeField(null=True)


class HistoricalPerformance(models.Model):
    objects = None
    vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE)
    date = models.DateTimeField()
    on_time_delivery_rate = models.FloatField()
    quality_rating_avg = models.FloatField()
    fulfilment_rate = models.FloatField()
