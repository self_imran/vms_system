from django.urls import path

from vms_app import views


urlpatterns = [

    # Vendor profile management
    path('vendors/', views.VendorAPIView.as_view()),
    path('vendors/<int:pk>/', views.VendorAPIView.as_view()),

    # historical performance
    path('vendors/performance/', views.get_performance, name="performance"),

    # purchase order
    path('purchase_orders/', views.PurchaseOrderAPIView.as_view()),
    path('purchase_orders/<int:pk>/', views.PurchaseOrderAPIView.as_view()),

]
