from rest_framework import serializers
from vms_app.models import Vendor, PurchaseOrder, HistoricalPerformance


class VendorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vendor
        fields = ('id', 'name', 'contact_details', 'address', 'vendor_code',
                  'on_time_delivery_rate', 'quality_rating_avg', 'average_response_time',
                  'fulfilment_rate')
        

class PurchaseOrderSerializer(serializers.ModelSerializer):
    vendor_code = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = PurchaseOrder
        fields = ('id', 'po_number', 'order_date', 'vendor', 'vendor_code', 'items', 'quantity', 'status',
                  'quality_rating', 'issue_date', 'acknowledgment_date')

    @staticmethod
    def get_vendor_code(obj):
        vendor_code = None
        try:
            vendor_code = Vendor.objects.filter(id=obj.vendor_id).values(
                'vendor_code'
            )[0]['vendor_code']
            print(vendor_code)

        except Exception:
            print("exception-->")
        return vendor_code
        

class HistoricalPerformanceSerializer(serializers.ModelSerializer):

    class Meta:
        model = HistoricalPerformance
        fields = ('id', 'date', 'on_time_delivery_rate', 'quality_rating_avg',
                  'fulfilment_rate')
