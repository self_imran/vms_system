import sys
from django.db import connections
import psycopg2

postgres_credential = {
    "host": "localhost",
    "database": "vmsdb",
    "username": "vmsdb_user",
    "password": "vmsdb_user"
}


def get_query_data(query):
    """

    @param query:
    @return:
    """
    conn_string = "host='{0}' dbname='{1}' user='{2}' password='{3}'"
    conn_string = conn_string.format(postgres_credential['host'], postgres_credential['database'],
                                     postgres_credential['username'],
                                     postgres_credential['password'])
    conn = ''
    try:
        conn = psycopg2.connect(conn_string)
        cur = conn.cursor()
        cur.execute(query)
        cursor = cur.fetchone()[0] if cur is not None else []
        return cursor
    except psycopg2.DatabaseError as e:
        print(e)
        sys.exit(1)

    finally:
        if conn:
            conn.close()
