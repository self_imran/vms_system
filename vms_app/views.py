from rest_framework import status
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response

from vms_app.utils import get_query_data
from vms_app.query import get_vms_query
from vms_app.models import Vendor, PurchaseOrder
from vms_app.serializer import VendorSerializer, PurchaseOrderSerializer


class VendorAPIView(APIView):

    @staticmethod
    def get(request):
        """

        :param request:
        :return:
        """
        serializer = None
        final_dict = {
            "tableHeader": ['id', 'name', 'contact_details', 'address', 'vendor_code',
                            'on_time_delivery_rate', 'quality_rating_avg', 'average_response_time', 'fulfilment_rate']
        }
        try:
            vendor_obj = Vendor.objects.all()
            serializer_context = {
                'request': request,
            }
            serializer = VendorSerializer(vendor_obj, many=True, context=serializer_context)
            final_dict['data'] = serializer.data
            return Response(final_dict)
        except Exception:
            return Response(serializer.error_messages, status=status.HTTP_204_NO_CONTENT)

    @staticmethod
    def post(request):
        if Vendor.objects.filter(vendor_code=request.data['vendor_code']).exists():
            return Response({"msg": "Vendor already exists"}, status=status.HTTP_302_FOUND)
        serializer = VendorSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def put(request, pk=None):
        vendor_obj = Vendor.objects.get(pk=pk)
        serializer = VendorSerializer(vendor_obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def delete(self, pk=None):
        try:
            vendor_obj = Vendor.objects.get(pk=pk)
            vendor_obj.delete()
            return Response({"msg": "Deleted"}, status=status.HTTP_204_NO_CONTENT)

        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)


class PurchaseOrderAPIView(APIView):

    @staticmethod
    def get(request):
        """

        :param request:
        :return:
        """
        serializer = None
        final_dict = {
            "tableHeader": ['id', 'po_number', 'order_date', 'items', 'quantity', 'status', 'quality_rating',
                            'issue_date', 'acknowledgment_date']
        }
        try:
            po_obj = PurchaseOrder.objects.all()
            serializer_context = {
                'request': request,
            }
            serializer = PurchaseOrderSerializer(po_obj, many=True, context=serializer_context)
            final_dict['data'] = serializer.data
            return Response(final_dict)
        except Exception:
            return Response(serializer.error_messages, status=status.HTTP_204_NO_CONTENT)

    @staticmethod
    def post(request):
        if PurchaseOrder.objects.filter(po_number=request.data['po_number']).exists():
            return Response({"msg": "Purchase order already created"}, status=status.HTTP_302_FOUND)
        serializer = PurchaseOrderSerializer(data=request.data)
        print(serializer)
        if serializer.is_valid():
            print(serializer.validated_data)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def put(request, pk=None):
        po_obj = PurchaseOrder.objects.get(pk=pk)
        serializer = PurchaseOrderSerializer(po_obj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @staticmethod
    def delete(self, pk=None):
        try:
            po_obj = PurchaseOrder.objects.get(pk=pk)
            po_obj.delete()
            return Response({"msg": "Deleted"}, status=status.HTTP_204_NO_CONTENT)

        except Exception as e:
            return Response({"error": str(e)}, status=status.HTTP_400_BAD_REQUEST)


@api_view(["GET"])
def get_performance(request):
    key_dict = {
        'on_time_delivery_rate': None,
        'quality_rating_avg': None,
        'average_response_time': None,
        'fulfillment_rate': None
    }
    for key, value in key_dict.items():
        query = get_vms_query(key)
        key_dict[key] = get_query_data(query)
    result_context = {
        'result': key_dict,
    }
    return Response(result_context, status=status.HTTP_200_OK)
